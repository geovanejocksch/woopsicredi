package com.example.woopsicredi.views.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import com.example.woopsicredi.R
import com.example.woopsicredi.models.People
import com.example.woopsicredi.views.adapters.PeoplesAdapter
import kotlinx.android.synthetic.main.fragment_people.*

class PeopleFragment(val people: List<People>) : Fragment() {

    private val GRID_SIZE = 3

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        return inflater.inflate(R.layout.fragment_people, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        configureRecycler()
    }

    private fun configureRecycler() {
        peopleRecycler.layoutManager = GridLayoutManager(requireContext(), GRID_SIZE)
        peopleRecycler.adapter = PeoplesAdapter(people)
    }
}