package com.example.woopsicredi

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.example.woopsicredi.models.Event
import com.example.woopsicredi.repositories.EventRepository
import com.example.woopsicredi.viewmodels.DetailViewModel
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import okhttp3.ResponseBody
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import retrofit2.Response

class DetailViewModelTest {

    private lateinit var viewModel: DetailViewModel
    private lateinit var repositoryMock: EventRepository

    @get:Rule
    var rule: TestRule = InstantTaskExecutorRule()

    @Before
    fun setup() {
        repositoryMock = mock()
        viewModel = DetailViewModel(repositoryMock)
    }

    @Test
    fun `get character and assert mutable is filled`() {

        // MOCK EXPECTED RESPONSE
        val EXPECTED_EVENT = Event(
            listOf(),
            64464L,
            "description",
            "12345678",
            "image.png",
            "latidude",
            "longitude",
            listOf(),
            25.0,
            "Event"
        )

        val response = Response.success(EXPECTED_EVENT)

        // MOCK SERVER RESPONSE
        whenever(
            repositoryMock.getEvent(any(), any())
        ).then {
            viewModel.callback.onResponse(
                mock(),
                response
            )
        }

        viewModel.getEvent("12345678")

        assertEquals(
            EXPECTED_EVENT,
            viewModel.event.value
        )
    }

    @Test
    fun `get character and handle unknown error`() {

        val responseBody = ResponseBody.create(
            mock(),
            "Unable to retrieve data"
        )

        val response = Response.error<Event>(
            500,
            responseBody
        )

        // MOCKING SERVICE RESPONSE
        whenever(
            repositoryMock.getEvent(any(), any())
        ).then {
            viewModel.callback.onResponse(
                mock(),
                response
            )
        }

        viewModel.getEvent("123")

        assertEquals(
            R.string.generic_error,
            viewModel.error.value
        )
    }

    @Test
    fun `call service and handle failure response`() {

        // MOCKING SERVICE RESPONSE
        whenever(
            repositoryMock.getEvent(any(), any())
        ).then {
            viewModel.callback.onFailure(
                mock(),
                mock()
            )
        }

        viewModel.getEvent("1234")

        assertEquals(
            R.string.generic_error,
            viewModel.error.value
        )
    }
}