package com.example.woopsicredi.views.activities

import android.content.Intent
import android.os.Bundle
import android.transition.TransitionManager
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.woopsicredi.R
import com.example.woopsicredi.models.Event
import com.example.woopsicredi.utils.EVENT_EXTRA_ID
import com.example.woopsicredi.viewmodels.MainViewModel
import com.example.woopsicredi.viewmodels.factory.MainViewModelFactory
import com.example.woopsicredi.views.adapters.EventsAdapter
import kotlinx.android.synthetic.main.activity_main.*
import org.koin.core.KoinComponent
import org.koin.core.get

class MainActivity : AppCompatActivity(), KoinComponent {

    private val mainViewModel by lazy {
        ViewModelProviders.of(this, MainViewModelFactory(get()))
            .get(MainViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        configureObservers()
        getList()

        ivReload.setOnClickListener {
            getList()
        }
    }

    private fun getList() {
        mainViewModel.getEventList()

        // Loading animation
        TransitionManager.beginDelayedTransition(llActivityMain)
        rvEvent.visibility = View.GONE
        llError.visibility = View.GONE
        lavLoader.visibility = View.VISIBLE
    }

    private fun configureObservers() {
        mainViewModel.events.observe(this, Observer {
            it?.let { nonNullEvent -> configureRecycler(nonNullEvent) }
        })

        mainViewModel.error.observe(this, Observer {
            if (it == null) {
                llError.visibility = View.GONE
                return@Observer
            }

            TransitionManager.beginDelayedTransition(llActivityMain)

            lavLoader.visibility = View.GONE
            llError.visibility = View.VISIBLE
            tvError.text = getString(it)
            rvEvent.visibility = View.GONE
        })
    }

    private fun configureRecycler(events: List<Event>) {
        rvEvent.layoutManager = LinearLayoutManager(this)
        rvEvent.adapter = EventsAdapter(events) { event, imageView ->
            val intent = Intent(this, EventDetailActivity::class.java).apply {
                putExtra(EVENT_EXTRA_ID, event.id)
            }

            startActivity(intent)
        }

        // Loading animation
        TransitionManager.beginDelayedTransition(llActivityMain)
        rvEvent.visibility = View.VISIBLE
        lavLoader.visibility = View.GONE
    }
}
