package com.example.woopsicredi.views.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.woopsicredi.R
import com.example.woopsicredi.databinding.ItemEventBinding
import com.example.woopsicredi.models.Event

class EventsAdapter(
    private val items: List<Event>,
    private val listener: (Event, ImageView) -> Unit
) :
    RecyclerView.Adapter<EventsAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = DataBindingUtil.inflate<ItemEventBinding>(
            LayoutInflater.from(parent.context),
            R.layout.item_event,
            parent,
            false
        )

        return ViewHolder(view, listener)
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(items[position])
    }

    class ViewHolder(
        private val binding: ItemEventBinding,
        private val listener: (Event, ImageView) -> Unit
    ) : RecyclerView.ViewHolder(binding.root) {

        fun bind(event: Event) {
            binding.event = event
            itemView.setOnClickListener { listener(event, binding.ivHeader) }
        }
    }
}