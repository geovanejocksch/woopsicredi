package com.example.woopsicredi.utils

import android.widget.ImageView
import android.widget.TextView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.example.woopsicredi.R
import java.text.DecimalFormat
import java.text.DecimalFormatSymbols
import java.util.Locale

internal const val currencyBR: String = "R$ 0.00"

internal const val headerWidth = 700
internal const val headerHeight = 300

internal const val photoWidth = 300
internal const val photoHeight = 300

@BindingAdapter("loadHeader")
fun ImageView.loadHeader(url: String?) {
    Glide.with(context)
        .load(url)
        .centerCrop()
        .apply(RequestOptions().override(headerWidth, headerHeight))
        .error(R.drawable.ic_image_not_found)
        .into(this)
}

@BindingAdapter("loadPhoto")
fun ImageView.loadPhoto(url: String?) {
    Glide.with(context)
        .load(url)
        .centerCrop()
        .apply(RequestOptions().override(photoWidth, photoHeight))
        .error(R.drawable.ic_image_not_found)
        .into(this)
}

@BindingAdapter("intToCurrency")
fun TextView.formatPriceBR(amount: Int) {
    val decimalFormat = DecimalFormat(
        currencyBR,
        DecimalFormatSymbols(Locale("pt", "BR"))
    )

    this.text = decimalFormat.format(amount)
}