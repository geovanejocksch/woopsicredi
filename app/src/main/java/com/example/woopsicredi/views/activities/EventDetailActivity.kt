package com.example.woopsicredi.views.activities

import android.os.Bundle
import android.os.Handler
import android.transition.TransitionManager
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.example.woopsicredi.R
import com.example.woopsicredi.databinding.ActivityEventDetailBinding
import com.example.woopsicredi.utils.EVENT_EXTRA_ID
import com.example.woopsicredi.viewmodels.DetailViewModel
import com.example.woopsicredi.viewmodels.factory.DetailViewModelFactory
import com.example.woopsicredi.views.adapters.TabsAdapter
import kotlinx.android.synthetic.main.activity_event_detail.*
import org.koin.core.KoinComponent
import org.koin.core.get

class EventDetailActivity : AppCompatActivity(), KoinComponent {

    private val viewModel by lazy {
        ViewModelProviders.of(this, DetailViewModelFactory(get()))
            .get(DetailViewModel::class.java)
    }

    private lateinit var binding: ActivityEventDetailBinding
    private var eventId: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_event_detail)

        // The API Object in both requests are the same. For simplicity,
        // it would be better to send the entire object parcelized and show here.
        // However, the test definition give both services, .
        eventId = intent.extras?.getString(EVENT_EXTRA_ID)
        eventId?.let { getEvent(eventId) }
        configureObservers()

        binding.ivReload.setOnClickListener {
            getEvent(eventId)
        }
    }

    private fun configureObservers() {
        viewModel.event.observe(this, Observer {

            it?.apply {
                TransitionManager.beginDelayedTransition(binding.llContent)

                binding.event = it
                binding.llContent.visibility = View.VISIBLE
                binding.appBarLayout.visibility = View.VISIBLE
                binding.lavLoader.visibility = View.GONE

                // Setup pager
                viewPager.adapter = TabsAdapter(it, supportFragmentManager, this@EventDetailActivity)
                tabLayout.setupWithViewPager(viewPager)
            }
        })

        viewModel.error.observe(this, Observer {
            if (it == null) {
                binding.llError.visibility = View.GONE
                return@Observer
            }

            TransitionManager.beginDelayedTransition(binding.llContent)
            binding.lavLoader.visibility = View.GONE
            binding.llError.visibility = View.VISIBLE
            binding.tvError.text = getString(it)
        })
    }

    private fun getEvent(id: String?) {
        id?.let {
            // Loading animation
            TransitionManager.beginDelayedTransition(binding.llContent)
            binding.llError.visibility = View.GONE
            binding.llContent.visibility = View.GONE
            binding.appBarLayout.visibility = View.GONE
            binding.lavLoader.visibility = View.VISIBLE

            // Request was so fast that we cannot see the loader,
            // Only for UX purpose, postDelay the call
            Handler().postDelayed({
                viewModel.getEvent(id)
            }, 800)
        }
    }
}