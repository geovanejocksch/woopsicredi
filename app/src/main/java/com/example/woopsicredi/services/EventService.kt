package com.example.woopsicredi.services

import com.example.woopsicredi.models.Event
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path

interface EventService {

    @GET("events")
    fun getEventList(): Call<List<Event>>

    @GET("events/{eventId}")
    fun getEvent(
        @Path("eventId") eventId: String
    ): Call<Event>
}