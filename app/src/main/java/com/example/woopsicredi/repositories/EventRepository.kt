package com.example.woopsicredi.repositories

import com.example.woopsicredi.datasources.EventDataSource
import com.example.woopsicredi.models.Event
import org.koin.core.get
import retrofit2.Callback

class EventRepository(private val dataSource: EventDataSource) {

    fun getEventList(callback: Callback<List<Event>>) {
        dataSource.getEventList(callback)
    }

    fun getEvent(id: String, callback: Callback<Event>) {
        dataSource.getEvent(id, callback)
    }
}