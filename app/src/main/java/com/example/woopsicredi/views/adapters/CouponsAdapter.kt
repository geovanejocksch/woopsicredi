package com.example.woopsicredi.views.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.woopsicredi.R
import com.example.woopsicredi.databinding.ItemCouponBinding
import com.example.woopsicredi.models.Cupon

class CouponsAdapter(
    private val items: List<Cupon>
) : RecyclerView.Adapter<CouponsAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = DataBindingUtil.inflate<ItemCouponBinding>(
            LayoutInflater.from(parent.context),
            R.layout.item_coupon,
            parent,
            false
        )

        return ViewHolder(binding)
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.onBind(items[position])
    }

    class ViewHolder(private val binding: ItemCouponBinding) : RecyclerView.ViewHolder(binding.root) {

        fun onBind(coupon: Cupon) {
            binding.coupon = coupon
        }
    }
}