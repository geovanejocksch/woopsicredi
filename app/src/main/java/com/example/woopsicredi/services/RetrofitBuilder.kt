package com.example.woopsicredi.services

import com.example.woopsicredi.BuildConfig
import com.example.woopsicredi.utils.BASE_URL
import com.example.woopsicredi.utils.DEFAULT_TIMEOUT
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

fun <T> getApi(serviceClass: Class<T>): T {

    // Configure okHttp client
    val client = OkHttpClient.Builder()
    client.readTimeout(DEFAULT_TIMEOUT, TimeUnit.SECONDS)
    client.connectTimeout(DEFAULT_TIMEOUT, TimeUnit.SECONDS)

    // Log only when debug
    if (BuildConfig.DEBUG) {
        val logging = HttpLoggingInterceptor()
        logging.level = HttpLoggingInterceptor.Level.BODY
        client.addInterceptor(logging)
    }

    // Build retrofit
    val retrofit = Retrofit.Builder()
        .baseUrl(BASE_URL)
        .client(client.build())
        .addConverterFactory(GsonConverterFactory.create())
        .build()

    return retrofit.create(serviceClass)
}