package com.example.woopsicredi.views.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.example.woopsicredi.R
import com.example.woopsicredi.databinding.FragmentInfoBinding
import com.example.woopsicredi.models.Event

class InfoFragment(private val event: Event) : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val binding = DataBindingUtil.inflate<FragmentInfoBinding>(
            inflater,
            R.layout.fragment_info,
            container,
            false
        )

        binding.event = event

        return binding.root
    }
}