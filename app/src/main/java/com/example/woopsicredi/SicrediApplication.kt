package com.example.woopsicredi

import android.app.Application
import com.example.woopsicredi.di.dataSourceModule
import com.example.woopsicredi.di.repositoryModule
import com.example.woopsicredi.di.serviceModule
import org.koin.core.context.startKoin

class SicrediApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        configureKoin()
    }

    private fun configureKoin() {
        startKoin {
            modules(
                listOf(
                    serviceModule,
                    dataSourceModule,
                    repositoryModule
                )
            )
        }
    }
}