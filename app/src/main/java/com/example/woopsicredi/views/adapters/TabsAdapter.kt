package com.example.woopsicredi.views.adapters

import android.content.Context
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import com.example.woopsicredi.R
import com.example.woopsicredi.models.Event
import com.example.woopsicredi.views.fragments.CouponsFragment
import com.example.woopsicredi.views.fragments.InfoFragment
import com.example.woopsicredi.views.fragments.PeopleFragment

class TabsAdapter(
    event: Event,
    fragmentManager: FragmentManager,
    context: Context
) :
    FragmentStatePagerAdapter(fragmentManager, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {

    private val tabs = listOf(
        InfoFragment(event),
        PeopleFragment(event.people),
        CouponsFragment(event.cupons)
    )

    private val tabsName = listOf(
        context.getString(R.string.tab_name_info),
        context.getString(R.string.tab_name_people),
        context.getString(R.string.tab_name_coupon)
    )

    override fun getItem(position: Int): Fragment = tabs[position]

    override fun getCount(): Int = tabs.size

    override fun getPageTitle(position: Int): CharSequence? {
        return tabsName[position]
    }
}