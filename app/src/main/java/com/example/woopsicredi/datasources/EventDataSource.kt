package com.example.woopsicredi.datasources

import com.example.woopsicredi.models.Event
import com.example.woopsicredi.services.EventService
import retrofit2.Callback

class EventDataSource(private val eventService: EventService) {

    fun getEventList(callback: Callback<List<Event>>) {
        eventService.getEventList()
            .enqueue(callback)
    }

    fun getEvent(id: String, callback: Callback<Event>) {
        eventService.getEvent(id)
            .enqueue(callback)
    }
}
