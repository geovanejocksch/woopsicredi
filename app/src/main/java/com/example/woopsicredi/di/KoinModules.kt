package com.example.woopsicredi.di

import com.example.woopsicredi.datasources.EventDataSource
import com.example.woopsicredi.repositories.EventRepository
import com.example.woopsicredi.services.EventService
import com.example.woopsicredi.services.getApi
import org.koin.dsl.module

val serviceModule = module {
    single { getApi(EventService::class.java) }
}

val dataSourceModule = module {
    factory { EventDataSource(get()) }
}

val repositoryModule = module {
    factory { EventRepository(get()) }
}