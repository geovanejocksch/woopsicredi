package com.example.woopsicredi.utils

const val DEFAULT_TIMEOUT = 60L
const val BASE_URL = "http://5b840ba5db24a100142dcd8c.mockapi.io/api/"

// Extras region
const val EVENT_EXTRA_ID = "event-extra"