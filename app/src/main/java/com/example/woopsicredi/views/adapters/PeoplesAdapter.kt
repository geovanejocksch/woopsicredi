package com.example.woopsicredi.views.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.woopsicredi.R
import com.example.woopsicredi.databinding.ItemPeopleBinding
import com.example.woopsicredi.models.People

class PeoplesAdapter(
    private val items: List<People>
) : RecyclerView.Adapter<PeoplesAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = DataBindingUtil.inflate<ItemPeopleBinding>(
            LayoutInflater.from(parent.context),
            R.layout.item_people,
            parent,
            false
        )

        return ViewHolder(binding)
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.onBind(items[position])
    }

    class ViewHolder(private val binding: ItemPeopleBinding) : RecyclerView.ViewHolder(binding.root) {

        fun onBind(people: People) {
            binding.people = people
        }
    }
}