package com.example.woopsicredi.views.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.woopsicredi.R
import com.example.woopsicredi.models.Cupon
import com.example.woopsicredi.views.adapters.CouponsAdapter
import kotlinx.android.synthetic.main.fragment_coupon.*

class CouponsFragment(private val coupons: List<Cupon>) : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        return inflater.inflate(R.layout.fragment_coupon, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        configureRecycler()
    }

    private fun configureRecycler() {
        couponRecycler.layoutManager = LinearLayoutManager(requireContext())
        couponRecycler.adapter = CouponsAdapter(coupons)
    }
}