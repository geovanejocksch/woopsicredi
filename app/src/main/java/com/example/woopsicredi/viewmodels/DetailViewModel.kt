package com.example.woopsicredi.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.woopsicredi.R
import com.example.woopsicredi.models.Event
import com.example.woopsicredi.repositories.EventRepository
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class DetailViewModel(private val repository: EventRepository) : ViewModel() {

    private val _event = MutableLiveData<Event>()
    val event: LiveData<Event> = _event

    private val _error = MutableLiveData<Int>()
    val error: LiveData<Int> = _error

    fun getEvent(id: String) {

        repository.getEvent(
            id, callback
        )
    }

    val callback = object : Callback<Event> {
        override fun onFailure(
            call: Call<Event>,
            t: Throwable
        ) {
            _error.value = R.string.generic_error
        }

        override fun onResponse(
            call: Call<Event>,
            response: Response<Event>
        ) {
            when (response.code()) {
                200 -> {
                    // Success
                    _event.value = response.body()
                }
                else -> {
                    // Generic Failure
                    _error.value = R.string.generic_error
                }
            }
        }
    }
}