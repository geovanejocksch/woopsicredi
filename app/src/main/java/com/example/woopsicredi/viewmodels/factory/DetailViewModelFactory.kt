package com.example.woopsicredi.viewmodels.factory

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.woopsicredi.repositories.EventRepository
import com.example.woopsicredi.viewmodels.DetailViewModel

class DetailViewModelFactory(
    private val repository: EventRepository
) : ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T =
        DetailViewModel(repository) as T
}