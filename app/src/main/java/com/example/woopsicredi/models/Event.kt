package com.example.woopsicredi.models

data class Event(
    val cupons: List<Cupon>,
    val date: Long,
    val description: String,
    val id: String,
    val image: String,
    val latitude: String,
    val longitude: String,
    val people: List<People>,
    val price: Double,
    val title: String
)

data class Cupon(
    val discount: Int,
    val eventId: String,
    val id: String
)

data class People(
    val eventId: String,
    val id: String,
    val name: String,
    val picture: String
)