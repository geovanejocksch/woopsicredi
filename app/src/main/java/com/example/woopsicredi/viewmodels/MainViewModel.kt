package com.example.woopsicredi.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.woopsicredi.R
import com.example.woopsicredi.models.Event
import com.example.woopsicredi.repositories.EventRepository
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MainViewModel(private val repository: EventRepository) : ViewModel() {

    private val _events = MutableLiveData<List<Event>>()
    val events: LiveData<List<Event>> = _events

    private val _error = MutableLiveData<Int>()
    val error: LiveData<Int> = _error

    fun getEventList() {
        repository.getEventList(callback)
    }

    val callback = object : Callback<List<Event>> {
        override fun onFailure(call: Call<List<Event>>, t: Throwable) {
            _error.value = R.string.generic_error
        }

        override fun onResponse(
            call: Call<List<Event>>,
            response: Response<List<Event>>
        ) {
            when (response.code()) {
                200 -> {
                    // Success
                    _events.value = response.body()
                    _error.value = null
                }
                else -> {
                    // Generic Failure
                    _error.value = R.string.generic_error
                }
            }
        }
    }
}