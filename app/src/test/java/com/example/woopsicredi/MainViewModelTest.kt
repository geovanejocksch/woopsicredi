package com.example.woopsicredi

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.example.woopsicredi.models.Event
import com.example.woopsicredi.repositories.EventRepository
import com.example.woopsicredi.viewmodels.MainViewModel
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import okhttp3.ResponseBody
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import retrofit2.Response
import org.junit.Assert.assertEquals

class MainViewModelTest {

    private lateinit var viewModel: MainViewModel
    private lateinit var repositoryMock: EventRepository

    @get:Rule
    var rule: TestRule = InstantTaskExecutorRule()

    @Before
    fun setup() {
        repositoryMock = mock()
        viewModel = MainViewModel(repositoryMock)
    }

    @Test
    fun `get list and assert mutable is filled`() {

        // MOCK EXPECTED RESPONSE
        val EXPECTED_EVENT = Event(
            listOf(),
            64464L,
            "description",
            "2",
            "image.png",
            "latidude",
            "longitude",
            listOf(),
            25.0,
            "Event"
        )

        val EXPECTED_LIST = listOf(EXPECTED_EVENT)

        val EXPECTED_RESPONSE = Response.success(200, EXPECTED_LIST)

        // MOCK SERVER RESPONSE
        whenever(
            repositoryMock.getEventList(any())
        ).then {
            viewModel.callback.onResponse(
                mock(),
                EXPECTED_RESPONSE
            )
        }

        viewModel.getEventList()

        assertEquals(
            viewModel.events.value,
            EXPECTED_LIST
        )
    }

    @Test
    fun `get list and handle unknown error`() {
        // MOCK EXPECTED RESPONSE
        val responseBody = ResponseBody.create(
            mock(),
            "Unable to retrieve data"
        )

        val response = Response.error<List<Event>>(
            500,
            responseBody
        )

        // MOCKING SERVICE RESPONSE
        whenever(
            repositoryMock.getEventList(any())
        ).then {
            viewModel.callback.onResponse(
                mock(),
                response
            )
        }

        viewModel.getEventList()

        assertEquals(
            R.string.generic_error,
            viewModel.error.value
        )
    }

    @Test
    fun `call service and handle failure response`() {
        // MOCKING SERVICE RESPONSE
        whenever(
            repositoryMock.getEventList(any())
        ).then {
            viewModel.callback.onFailure(
                mock(),
                mock()
            )
        }

        viewModel.getEventList()

        assertEquals(
            R.string.generic_error,
            viewModel.error.value
        )
    }
}